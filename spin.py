"""
spin.py
Raphael Salazar
19/05/2021

Calculates the spin precession with a given magnetic field
syntax : spin.py {theta} {phi} {spin_orientation}
theta and phi angles in spherical coordinates, in radian
spin_orientation can take the values x y or z  
"""

from sympy import *
from sympy.parsing.sympy_parser import parse_expr

from sys import argv
import re as reg

import numpy as np
from scipy.linalg import expm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt

#Definitions of Pauli matrices

sx = Matrix([[0,1],[1,0]])
sy = Matrix([[0,-I],[I,0]])
sz = Matrix([[1,0],[0,-1]])

Sx = sx/2
Sy = sy/2
Sz = sz/2

spin_dict = {'x':Sx,'y':Sy,'z':Sz}

#Definition of variables as symbols objects
t, theta, phi, Omega= symbols('t theta phi Omega',real=True)

if len(argv) >= 4:
    for i in range(len(argv)):#Arguments taken from console
        if i == 1 :
            theta = parse_expr(argv[i])
        if i == 2 :
            phi = parse_expr(argv[i])
        if i == 3 :
            if argv[i].isnumeric():
                print('It is '+str(argv[i].isnumeric())+' that argv[3] is an numeral')
                print('Default orientation for initial spin is set to +y')
                spin_mat = Sy
            else :
                match = False
                for key in spin_dict.keys():
                    if reg.match(key,argv[i]):
                        spin_mat = spin_dict[key]
                        print('Default orientation for initial spin is set to +'+argv[i])
                        match = True

                if not match :
                    print('It is '+str(argv[i].isnumeric())+' that argv[3] is an numeral')
                    print('Key \"'+argv[i]+'\" is invalid')
                    print('Default orientation for initial spin is set to +y')
                    spin_mat = Sy
                        

    print('\n\n')
    #Orientation vector of the magnetic field
    n = Matrix([sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)])

    #Resulting hamiltonian, simplified so that cos(phi)+i*sin(phi)=exp(i*phi)
    H = simplify(nsimplify(-1*(n[0]*sx + n[1]*sy + n[2]*sz), rational = True))

    spin_up = Matrix([cos(theta/2),exp(I*phi)*sin(theta/2)])#up/down spinors definition
    spin_down = Matrix([sin(theta/2),-exp(I*phi)*cos(theta/2)])

    psi0 = spin_mat.eigenvects()[1][2][0]
    psi0 = psi0/psi0.norm()
    #The spin vector (+) is oriented along the Y axis
    #eigenvects format = [(eigenvalue1:algebraic multiplicity, [eigenvectors]),(eigenvalue2...)]
    psi = simplify((expand(exp(-I*Omega*H*t)*psi0))) 

    mean_Sx = psi.conjugate().transpose()*Sx*psi#Usual product is matrix product
    mean_Sy = psi.conjugate().transpose()*Sy*psi
    mean_Sz = psi.conjugate().transpose()*Sz*psi

    mean_S = Matrix([mean_Sx[0],mean_Sy[0],mean_Sz[0]])#Components are encapsulated in a tuple
    mean_S = (re(trigsimp(expand(mean_S))))

    print('Orientation of B:')
    pprint(n)
    print('\nResulting Hamiltonian:')
    pprint(H)
    print('\nValue of psi(0):')
    pprint(psi0)
    pprint('\nValue of psi:')
    pprint(psi)
    print('\nMean value of Spin vector:')
    pprint(mean_S)

    ##--------------------------------
    #Generating the '\\begin{equation}'+latex file

    file = open('fichier.tex','w')
    file.write(
    """    \\documentclass{report}
        % General document formatting
        \\usepackage[margin=0.7in]{geometry}
        \\usepackage[parfill]{parskip}
        \\usepackage[utf8]{inputenc}
        \\usepackage[justification=centering]{caption}
        \\usepackage{subcaption}
        \\usepackage{pdfpages}
        
        \\usepackage{graphicx}
        
        % Related to math
        \\usepackage{amsmath,amssymb,amsfonts,amsthm}
        \\usepackage{braket}
        \\usepackage[thinc]{esdiff} 

    \\begin{document}
    """)
    file.write('The magnetic field B is oriented on direction n\n')
    file.write('\\begin{equation}'+latex(n)+'\n\\end{equation}')
    file.write('\nThe full Hamiltonian writes\n')
    file.write('\\begin{equation}'+latex(H)+'\n\\end{equation}')
    file.write('\nIts eigenvectors are :\n')
    file.write('\\begin{equation}'+latex(spin_up)+'\n\\end{equation}')
    file.write('\\begin{equation}'+latex(spin_down)+'\n\\end{equation}')
    file.write('\nThe state vector writes :\n')
    file.write('\\begin{equation}'+latex('\\psi(t) = e^{-iHt}\\psi(0)'+'\n\\end{equation}'))
    file.write('\nWith $\\psi(0)$ being :\n')
    file.write('\\begin{equation}'+latex(psi0)+'\n\\end{equation}')
    file.write('\\begin{equation}'+latex(psi)+'\n\\end{equation}')
    file.write('\nTherefore the spin mean value is :\n')
    file.write('\\begin{equation}'+latex(mean_S)+'\n\\end{equation}')
    file.write('\n')
    file.write('\\end{document}')
    file.close()

    ##--------------------------------
    #Plotting the precession of the spin

    if type(theta) != type(t) and type(phi) != type(t):

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        vecs = []

        N = 20
        psi0_n = np.array(psi0).astype(complex)
        H_n = np.array(H).astype(complex)
        S = np.array([np.array(Sx).astype(complex), np.array(Sy).astype(complex), np.array(Sz).astype(complex)])

        for k in range(N+1):
            psi_k = np.dot(expm(-1j*H_n*k*np.pi/N),psi0_n)
            mean_S = np.array([np.real(np.dot(np.conjugate(psi_k.T), np.dot(s,psi_k))[0][0]) for s in S])
            vecs.append(mean_S/np.sqrt(np.dot(mean_S.T,mean_S)))

        vecs = np.array(vecs)
        #vecs = np.array([mean_S.subs([(t,k*pi/(N*Omega))]) for k in range(0, N+1)])
        spin = ax.quiver(0,0,0,vecs[0][0],vecs[0][1],vecs[0][2],label = 'Initial Spin', color = (0,0,1))
        magField = ax.quiver(0,0,0,n[0],n[1],n[2],label = 'Magnetic Field $(\\theta ='+argv[1]+', \\phi ='+argv[2]+')$',color='r')
        k=2
        for i in vecs[1:] :
            ax.quiver(0,0,0,i[0],i[1],i[2],color=(0,0,1,1/k))
            k+=1

        ax.plot3D(vecs.transpose()[0],vecs.transpose()[1],vecs.transpose()[2],'.--')
        ax.legend()
        ax.set_xlim3d(-1, 1)
        ax.set_xlabel('x')
        ax.set_ylim3d(-1, 1)
        ax.set_ylabel('y')
        ax.set_zlim3d(-1, 1)
        ax.set_zlabel('z')
        plt.show()

else :
    print('Please give all arguments in the following fashion :\npython3 spin.py {theta} {phi} {axis}')
    print('example: python3 spin.py pi/4 pi/2 y')
